//
//  GameData.swift
//  SoccerPong
//
//  Created by Cristiano Douglas on 20/06/17.
//  Copyright © 2017 Cristiano Douglas. All rights reserved.
//

import Foundation

/*********************************************************************************************/
// MARK: GameData
/*********************************************************************************************/
class GameData {
	
	/*****************************************************************************************/
	
	var playerScore		: Int			= 1
	var enemyScore		: Int			= 7
	
	var gameLevel		: Int			= 1
	var ballSpeed		: Float			= 1.025
	var enemyDelay		: TimeInterval	= 0.1

	var soundEnabled	: Bool			= true
	
	/*****************************************************************************************/
	
	static let PortfoliumURL	: String! = "https://sites.google.com/site/cristianoprogrammer/"
	
	/*****************************************************************************************/
	
	func resetScore () {
		
		self.playerScore	= 0
		self.enemyScore		= 0
		
	}
	
	/*****************************************************************************************/
	
}
