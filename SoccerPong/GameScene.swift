//
//  GameScene.swift
//  SoccerPong
//
//  Created by Cristiano Douglas on 18/06/17.
//  Copyright © 2017 Cristiano Douglas. All rights reserved.
//

import SpriteKit
import GameplayKit

/*********************************************************************************************/
// MARK: GameScene : SKScene
/*********************************************************************************************/
class GameScene: SKScene, SKPhysicsContactDelegate {
	
	/*****************************************************************************************/
	
    private var lastUpdateTime	: TimeInterval = 0
	
	// Sprite
	private var sptField		= SKSpriteNode()
	private var sptScoreboard	= SKSpriteNode()
	private var sptBall			= SKSpriteNode()
	private var sptPad1			= SKSpriteNode()
	private var sptPad2			= SKSpriteNode()
	
	// Label
	private var lblScore1		= SKLabelNode()
	private var lblScore2		= SKLabelNode()
	
	// Audio
	private var soundMenu		= SKAudioNode()
	private var soundStadium	= SKAudioNode()
	
	// Shape
    private var trailNode		: SKShapeNode?
	
	// PhysicBody
	private var physicsBorder	: SKPhysicsBody?
	
	// Point
	private var pntInitBall		= CGPoint(x: 0.0, y: -25.0)
	
	// Vector
	private var vecBallInit		= CGVector(dx: -20.0, dy: -20)
	private var vecBallPlay		= CGVector(dx:  20.0, dy:  20)
	private var vecBallEnem		= CGVector(dx: -20.0, dy: -20)
	
	// Color
	private var colorBall		= SKColor.white
	
	
	// State
	var gameState				= GameState.Menu
	
	// Data
	var gameData				= GameData()
	
	
	/*****************************************************************************************/
	
	/*-----------------------------------------------------------------*/
	// MARK: Game Scene Loader
	/*-----------------------------------------------------------------*/
    override func sceneDidLoad() {

        self.lastUpdateTime = 0
		
		// Get Sprite
		self.sptField		= (self.childNode(withName: "//sptField")		as? SKSpriteNode)!
		self.sptScoreboard	= (self.childNode(withName: "//sptScoreboard")	as? SKSpriteNode)!
		self.sptBall		= (self.childNode(withName: "//sptBall")		as? SKSpriteNode)!
		self.sptPad1		= (self.childNode(withName: "//sptPad1")		as? SKSpriteNode)!
		self.sptPad2		= (self.childNode(withName: "//sptPad2")		as? SKSpriteNode)!
		
		// Get Label
		self.lblScore1		= (self.childNode(withName: "//lblScore1")		as? SKLabelNode)!
		self.lblScore2		= (self.childNode(withName: "//lblScore2")		as? SKLabelNode)!
		
		// Initial ball impulse
		self.sptBall.position						= self.pntInitBall
		self.sptBall.physicsBody?.angularVelocity  += 1.0
		
		// Border physic body
		self.physicsBorder				= SKPhysicsBody(edgeLoopFrom: self.frame)
		self.physicsBorder?.friction	= 0
		self.physicsBorder?.restitution = 1
		self.physicsBody				= self.physicsBorder
		
		// Physics world
		self.physicsWorld.contactDelegate = self;
		
        // Config ball trail
		self.trailNode = SKShapeNode.init(circleOfRadius: self.sptBall.size.width/2)
        if let trailNode = self.trailNode {
			
            trailNode.lineWidth = 2.0
            trailNode.run(SKAction.repeatForever(SKAction.rotate(byAngle: CGFloat(Double.pi), duration: 1)))
            trailNode.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                              SKAction.fadeOut(withDuration: 0.5),
                                              SKAction.removeFromParent()]))
        }
		
		
		//self.resetGame()
		self.resetBall()
    }
	
	/*.................................................*/
	
	override func didMove(to view: SKView) {
		
		self.prepareToStartGame()
		
	}
	
	/*-----------------------------------------------------------------*/
	// MARK: Audio actions
	/*-----------------------------------------------------------------*/
	
	func playBallSound () {
		
		self.run(SKAction.playSoundFileNamed("Bouncy.m4a", waitForCompletion: false))
		
	}
	
	/*-----------------------------------------------------------------*/
	// MARK: Game actions
	/*-----------------------------------------------------------------*/
	
	/**
		Set game state
	*/
	func setGameState(state : GameState) {
		self.gameState = state
	}
	
	/*.................................................*/
	
	/**
		Set game level
	*/
	func setGameLevel(level : Int) {
		
		self.gameData.gameLevel = level
		self.setEnemySpeed()
		self.setBallSpeed()
	}
	
	/*.................................................*/
	
	/**
		Set enemy move delay by game level
	*/
	func setEnemySpeed() {
		
		var speed = 0.0
		switch self.gameData.gameLevel {
		case 1:
			speed = 0.4
		case 2:
			speed = 0.16
		case 3:
			speed = 0.07
		default:
			speed = 0.4
		}
		
		self.gameData.enemyDelay = speed
	}
	
	/*.................................................*/
	
	/**
		Set ball increase speed by game level
	*/
	func setBallSpeed() {
		
		var impulse : CGFloat = 0.0
		switch self.gameData.gameLevel {
		case 1:
			self.gameData.ballSpeed = 1.002
			impulse = 10.0
		case 2:
			self.gameData.ballSpeed = 1.025
			impulse = 20.0
		case 3:
			self.gameData.ballSpeed = 1.05
			impulse = 25.0
		default:
			self.gameData.ballSpeed = 1.002
			impulse = 10.0
		}
		
		self.vecBallPlay		= CGVector(dx:  impulse, dy:  impulse)
		self.vecBallEnem		= CGVector(dx: -(impulse), dy: -(impulse))
		
	}
	
	/*.................................................*/
	
	/**
		Reload game
	*/
	func reload() {
		
		self.resetGame()
		self.setGameState(state: .InGame)
		self.prepareToStartGame()
		
	}
	
	/*.................................................*/
	
	/**
		Reset all game param's
	*/
	func resetGame() {
		
		self.gameData.resetScore()
		self.resetBall()
		self.sptPad1.run(SKAction.moveTo(x: 0.0, duration: 0.3))
		self.sptPad2.run(SKAction.moveTo(x: 0.0, duration: 0.3))
		self.colorBall = self.sptPad1.color
		self.vecBallInit = self.vecBallPlay
		
	}
	
	/*.................................................*/
	
	/**
		Reset ball position
	*/
	func resetBall () {
		
		self.sptBall.position = self.pntInitBall
		self.sptBall.physicsBody?.isDynamic = false
		self.sptBall.physicsBody?.angularVelocity = 1.0
	}
	
	/*.................................................*/
	
	/**
		Start ball after 1.5 secconds
	*/
	func prepareToStartGame() {
		
		self.perform(#selector(self.startGame), with: nil, afterDelay: 1.0)
		
	}
	
	/*.................................................*/
	
	/**
		Start ball
	*/
	func startGame() {
		
		self.sptBall.run(SKAction.playSoundFileNamed("Whistle.m4a", waitForCompletion: false))
		self.sptBall.physicsBody?.isDynamic = true
		self.sptBall.physicsBody?.applyImpulse(vecBallInit)
	}
	
	/*.................................................*/
	
	/**
		Add a score to player or enemy
	*/
	func addGameScore(pad : SKSpriteNode) {
		
		if (pad == self.sptPad1) {
			
			self.gameData.playerScore += 1
			
		} else {
			
			self.gameData.enemyScore += 1
			
		}
		
		if ( self.gameData.enemyScore >= 7 || self.gameData.playerScore >= 7) {
			
			self.reload()
			return
		}
		
		self.resetBall()
		self.prepareToStartGame()
		
	}
	
	/*-----------------------------------------------------------------*/
	// MARK: Touch Update
	/*-----------------------------------------------------------------*/
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for touch in touches {
		
			// Touch location
			let location = touch.location(in: self)
			
			/*.................................................*/
			// MARK: Game Animation
			/*.................................................*/
			if (self.gameState == .InGame) {
				
				// Pad 1 moviment
				self.sptPad1.run(SKAction.moveTo(x: location.x, duration: 0.3))
			}
		
		}
    }
	
	/*.................................................*/
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
		
        for touch in touches {
			
			// Touch location
			let location = touch.location(in: self)
			
			/*.................................................*/
			// MARK: Game Animation
			/*.................................................*/
			if (self.gameState == .InGame) {
				
				// Pad 1 moviment
				self.sptPad1.run(SKAction.moveTo(x: location.x, duration: 0.3))
			}
			
		}
    }
	
	/*.................................................*/
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for _ in touches {

		}
    }
	
	/*.................................................*/
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for _ in touches {
		
		}
    }
    
    /*-----------------------------------------------------------------*/
	// MARK: Game Update
	/*-----------------------------------------------------------------*/
    override func update(_ currentTime: TimeInterval) {
		
        if (self.lastUpdateTime == 0) {
            self.lastUpdateTime = currentTime
        }
		
		self.updateTrail(position: self.sptBall.position, time: currentTime)
		
		/*.................................................*/
		// Menu Update
		/*.................................................*/
		if (self.gameState == .Menu) {
			
			// Pad 1 animation
			if ((self.sptBall.position.y) < 0) {
				self.sptPad1.run(SKAction.moveTo(x: (self.sptBall.position.x), duration: 0.2))
			} else {
				self.sptPad1.run(SKAction.moveTo(x: 0.0, duration: 0.2))
			}
			
			//Pad 2 animation
			if ((self.sptBall.position.y) > 0) {
				self.sptPad2.run(SKAction.moveTo(x: (self.sptBall.position.x), duration: 0.2))
			} else {
				self.sptPad2.run(SKAction.moveTo(x: 0.0, duration: 0.2))
			}
			
			self.updateGameScore()
		}
		/*.................................................*/
		// Game Updates
		/*.................................................*/
		else if (self.gameState == .InGame) {
			
			//Pad 2 animation
			if ((self.sptBall.position.y) > 0) {
				self.sptPad2.run(SKAction.moveTo(x: (self.sptBall.position.x), duration: self.gameData.enemyDelay))
			} else {
				self.sptPad2.run(SKAction.moveTo(x: 0.0, duration: self.gameData.enemyDelay))
			}
			
			self.updateBall()
			self.updateGameScore()
		}
		
		
	}
	
	/*.................................................*/
	
	/**
		Verify if one player did a goal
	*/
	func updateBall() {
		
		// Enemy Goal
		if (self.sptBall.position.y < (self.sptPad1.position.y-(sptPad1.size.height*2.0))) {
			
			self.colorBall = self.sptPad2.color
			self.vecBallInit = vecBallEnem
			self.addGameScore(pad: self.sptPad2)
			
		}
		// Player Goal
		else if (self.sptBall.position.y > (self.sptPad2.position.y+(sptPad2.size.height*2.0))) {
			
			self.colorBall = self.sptPad1.color
			self.vecBallInit = vecBallPlay
			self.addGameScore(pad: self.sptPad1)
			
		}
		
		let velocity = (self.sptBall.physicsBody?.velocity)!
		if (velocity.dy < 3.0 && velocity.dy > -3.0 ) {
			//self.resetBall()
			//self.prepareToStartGame()
		}
		
	}
	
	/*.................................................*/
	
	/**
		Update game score
	*/
	func updateGameScore() {
		
		self.lblScore1.text = "\(self.gameData.playerScore)"
		self.lblScore2.text = "\(self.gameData.enemyScore)"
		
	}
	
	/*.................................................*/
	
	/**
		Update ball trail
	*/
	func updateTrail(position : CGPoint, time : TimeInterval) {
		
		let dt = time - self.lastUpdateTime
		
		if (dt > 0.025) {
			if let node = self.trailNode?.copy() as! SKShapeNode? {
				
				node.position = position
				node.zPosition = 3.0
				node.strokeColor = self.colorBall
				
				self.addChild(node)
				self.lastUpdateTime = time
			
			}
		}
		
	}
	
	/*-----------------------------------------------------------------*/
	// MARK: PhysicsDelegate
	/*-----------------------------------------------------------------*/
	
	func didBegin(_ contact: SKPhysicsContact) {
		
		if ( contact.bodyA.categoryBitMask == 1 ) {
			
			self.colorBall = ((contact.bodyA.node as? SKSpriteNode)?.color)!
			
			if (self.gameState == .InGame) {
				self.sptBall.physicsBody?.angularVelocity += 1.0
				let velocity = (self.sptBall.physicsBody?.velocity)! * CGFloat(self.gameData.ballSpeed)
				self.sptBall.physicsBody?.velocity = velocity
				self.sptBall.run(SKAction.playSoundFileNamed("Bouncy.m4a", waitForCompletion: false))
			}
		}
		
	}
	
	
	/*****************************************************************************************/
}
