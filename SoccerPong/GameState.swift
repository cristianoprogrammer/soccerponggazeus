//
//  GameState.swift
//  SoccerPong
//
//  Created by Cristiano Douglas on 20/06/17.
//  Copyright © 2017 Cristiano Douglas. All rights reserved.
//

import Foundation


/*********************************************************************************************/
// MARK: GameState
/*********************************************************************************************/
enum GameState : String {
	
	/*****************************************************************************************/
	
	case Menu	 = "Menu"
	case Settings = "Settings"
	case InGame	 = "InGame"
	case Pause	 = "Pause"
	
	/*****************************************************************************************/
	
}
