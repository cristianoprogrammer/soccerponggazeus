//
//  GameViewController.swift
//  SoccerPong
//
//  Created by Cristiano Douglas on 18/06/17.
//  Copyright © 2017 Cristiano Douglas. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import AVFoundation

/*********************************************************************************************/
// MARK: GameViewController: UIViewController
/*********************************************************************************************/
class GameViewController: UIViewController {
	
	/*****************************************************************************************/
	
	@IBOutlet weak var viewBlur				: UIVisualEffectView!
	@IBOutlet weak var viewMenu				: UIView!
	
	@IBOutlet weak var imgMenuBackground	: UIImageView!
	@IBOutlet weak var btnPlay				: UIButton!
	@IBOutlet weak var btnSettings			: UIButton!
	@IBOutlet weak var btnPortfolium		: UIButton!
	@IBOutlet weak var btnGazeus			: UIButton!
	
	@IBOutlet weak var btnEasy				: UIButton!
	@IBOutlet weak var btnMedium			: UIButton!
	@IBOutlet weak var btnHard				: UIButton!
	
	
	@IBOutlet weak var viewGame				: UIView!
	@IBOutlet weak var btnPausePlay			: UIButton!
	
	var audioMenu							: AVAudioPlayer!
	var audioStadium						: AVAudioPlayer!
	
	var gameSceneNode						: GameScene?
	
	/*****************************************************************************************/
	
	/*-----------------------------------------------------------------*/
	// MARK: UIViewController
	/*-----------------------------------------------------------------*/
    override func viewDidLoad() {
        super.viewDidLoad()
        
		// Game
        if let scene = GKScene(fileNamed: "GameScene") {
			
            // Get the SKScene from the loaded GKScene
            if let sceneNode = scene.rootNode as! GameScene? {
                
                // Copy gameplay related content over to the scene
                //sceneNode.entities = scene.entities
                //sceneNode.graphs = scene.graphs
                
                // Set the scale mode to scale to fit the window
                sceneNode.scaleMode = .aspectFill
                
                // Present the scene
                if let view = self.view as! SKView? {
                    view.presentScene(sceneNode)
                    
                    view.ignoresSiblingOrder = true
                    
                    view.showsFPS = false
                    view.showsNodeCount = false
                }
				
				self.gameSceneNode = sceneNode
            }
        }
		
		// Views
		self.viewBlur.alpha			= 0.85
		self.viewMenu.alpha			= 0.0
		self.btnPlay.alpha			= 0.0
		self.btnSettings.alpha		= 0.0
		self.btnPortfolium.alpha	= 0.0
		self.btnGazeus.alpha		= 0.0
		self.btnEasy.alpha			= 0.0
		self.btnMedium.alpha		= 0.0
		self.btnHard.alpha			= 0.0
		
		self.viewGame.alpha			= 0.0
		self.btnPausePlay.alpha		= 1.0
		
		// Audio
		do {
			self.audioMenu	 = try AVAudioPlayer(contentsOf: Bundle.main.url(forResource: "MenuSound", withExtension: "mp3")!)
			self.audioStadium = try AVAudioPlayer(contentsOf: Bundle.main.url(forResource: "Stadium", withExtension: "mp3")!)
		} catch {
		}
		
		self.audioMenu.numberOfLoops	= -1
		self.audioStadium.numberOfLoops = -1
	}
	
	/*.................................................*/
	
	override func viewDidAppear(_ animated: Bool) {
		
		self.perform(#selector(self.showViewMenu), with: nil, afterDelay: 1.5)
		
	}
	
	/*.................................................*/
	
	override var shouldAutorotate: Bool {
		return false
	}
	
	/*.................................................*/
	
	override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
		if UIDevice.current.userInterfaceIdiom == .phone {
			return .allButUpsideDown
		} else {
			return .all
		}
	}
	
	/*.................................................*/
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Release any cached data, images, etc that aren't in use.
	}
	
	/*.................................................*/
	
	override var prefersStatusBarHidden: Bool {
		return true
	}
	
	/*-----------------------------------------------------------------*/
	// MARK: Animation
	/*-----------------------------------------------------------------*/
	
	func showMenu () {
		
		UIView.animate(withDuration: 0.5, animations: {
				self.viewGame.alpha = 0.0
				self.viewBlur.alpha = 0.9
			
		}) { (completed : Bool) in
			
			self.showViewMenu()
			
		}
	}
	
	/*.................................................*/
	
	func showViewMenu () {
		
		UIView.animate(withDuration: 1.5, animations: {
			self.gameSceneNode?.isPaused = false
			self.viewMenu.alpha = 1.0
			
		}) { (completed : Bool) in
			
			self.audioMenu.prepareToPlay()
			self.audioMenu.play()
			
			UIView.animate(withDuration: 1.5) {
				self.btnPlay.alpha			= 1.0
				self.btnSettings.alpha		= 1.0
				self.btnPortfolium.alpha	= 1.0
				self.btnGazeus.alpha		= 1.0
			}
			UIView.animate(withDuration: 2.0) {
				self.btnGazeus.alpha		= 1.0
			}
		}
	}
	
	/*.................................................*/
	
	func showSelectLevel () {
		
		UIView.animate(withDuration: 0.5, animations: {
			self.btnPlay.alpha			= 0.0
			self.btnSettings.alpha		= 0.0
			self.btnPortfolium.alpha	= 0.0
			
		}) { (completed : Bool) in
			
			UIView.animate(withDuration: 0.5, animations: {
				self.btnEasy.alpha			= 1.0
				self.btnMedium.alpha		= 1.0
				self.btnHard.alpha			= 1.0
			}) { (completed : Bool) in
				
			}
		}
		
	}
		
	/*.................................................*/
	
	func showGame () {
		
		self.audioStadium.prepareToPlay()
		UIView.animate(withDuration: 0.5, animations: {
			
			self.btnEasy.alpha			= 0.0
			self.btnMedium.alpha		= 0.0
			self.btnHard.alpha			= 0.0
			self.btnGazeus.alpha		= 0.0
			
		}) { (completed : Bool) in
			
			self.audioMenu.stop()
			self.audioStadium.play()
			self.gameSceneNode?.resetGame()
			self.gameSceneNode?.setGameState(state: .InGame)
			
			UIView.animate(withDuration: 0.5, animations: {
				
				self.viewBlur.alpha		= 0.0
				self.viewMenu.alpha		= 0.0
				self.viewGame.alpha		= 1.0
				
			}) { (completed : Bool) in
				
				self.gameSceneNode?.prepareToStartGame()
				
			}
			
		}
	}
	
	/*-----------------------------------------------------------------*/
	// MARK: Menu Actions
	/*-----------------------------------------------------------------*/
	
	@IBAction func actionPlay(_ sender: Any) {
		
		self.gameSceneNode?.playBallSound()
		self.showSelectLevel()
	}
	
	/*.................................................*/

	@IBAction func actionSettings(_ sender: Any) {
		
		self.gameSceneNode?.playBallSound()
	}
	
	/*.................................................*/
	
	@IBAction func actionPortfolium(_ sender: Any) {
		
		self.gameSceneNode?.playBallSound()
		UIApplication.shared.open(URL.init(string: GameData.PortfoliumURL)!, options: [:], completionHandler: nil)
	}
	
	/*-----------------------------------------------------------------*/
	// MARK: Game level Actions
	/*-----------------------------------------------------------------*/
	
	@IBAction func actionEasy(_ sender: Any) {
		
		self.gameSceneNode?.playBallSound()
		self.gameSceneNode?.setGameLevel(level: 1)
		self.showGame()
		
	}
	
	/*.................................................*/
	
	@IBAction func actionMedium(_ sender: Any) {
		
		self.gameSceneNode?.playBallSound()
		self.gameSceneNode?.setGameLevel(level: 2)
		self.showGame()
	}
	
	/*.................................................*/
	
	@IBAction func actionHard(_ sender: Any) {
		
		self.gameSceneNode?.playBallSound()
		self.gameSceneNode?.setGameLevel(level: 3)
		self.showGame()
	}
	
	
	/*-----------------------------------------------------------------*/
	// MARK: Game Actions
	/*-----------------------------------------------------------------*/
	@IBAction func actionPausePlay(_ sender: Any) {
		
		if (self.btnPausePlay.isSelected) {
			
			self.gameSceneNode?.isPaused = false
			self.btnPausePlay.isSelected = false
			self.gameSceneNode?.setGameState(state: .InGame)
			
		} else {
			
			self.gameSceneNode?.isPaused = true
			self.btnPausePlay.isSelected = true
			self.gameSceneNode?.setGameState(state: .Menu)
		}
	}
	
	/*.................................................*/
	
	@IBAction func actionMenu(_ sender: Any) {
		
		self.gameSceneNode?.setGameLevel(level: 1)
		self.gameSceneNode?.isPaused = true
		self.audioStadium.stop()
		
		self.showMenu()
		self.showViewMenu()
		
		self.gameSceneNode?.resetGame()
		self.gameSceneNode?.setGameState(state: .Menu)
		self.gameSceneNode?.prepareToStartGame()
		
	}
	
	/*.................................................*/
	
	@IBAction func actionReload(_ sender: Any) {
		
		self.gameSceneNode?.reload()
		
	}
	
	/*****************************************************************************************/
}
